# PackingClient

Ensi Packing

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: 1.0.0
- Build package: org.openapitools.codegen.languages.PhpClientCodegen
For more information, please visit [https://ensi.tech/contacts](https://ensi.tech/contacts)

## Requirements

PHP 5.5 and later

## Installation & Usage

### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/greensight/ensi/oms/clients/packing-client-php.git"
    }
  ],
  "require": {
    "ensi/packing-client": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/PackingClient/vendor/autoload.php');
```

## Tests

To run the unit tests:

```bash
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Ensi\PackingClient\Api\ExamplesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$include = 'include_example'; // string | Связанные сущности для подгрузки, через запятую

try {
    $result = $apiInstance->getExample($id, $include);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ExamplesApi->getExample: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *http://localhost/api/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ExamplesApi* | [**getExample**](docs/Api/ExamplesApi.md#getexample) | **GET** /examples/{id} | Получение объекта типа Example


## Documentation For Models

 - [EmptyDataResponse](docs/Model/EmptyDataResponse.md)
 - [Error](docs/Model/Error.md)
 - [ErrorResponse](docs/Model/ErrorResponse.md)
 - [PaginationTypeEnum](docs/Model/PaginationTypeEnum.md)


## Documentation For Authorization

All endpoints do not require authorization.

## Author

mail@greensight.ru

